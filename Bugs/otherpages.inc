<p>D'altres p�gines del BTS:

<ul>
  <li><a href="./">P�gina principal de continguts del sistema de seguiment d'errors.</a>
  <li><a href="Reporting">Instruccions per a reportar errors.</a>
  <li><a href="Access">Accedint a la bit�cola del sistema de seguiment d'errors.</a>
  <li><a href="Developer">Informaci� per als desenvolupadors sobre el sistema de
      seguiment d'errors.</a>
  <li><a href="server-control">Informaci� per als desenvolupadors de com usar la
      interf�cie de correu per al control dels errors.</a>
  <li><a href="server-refcard">Targeta de refer�ncia pel servidor de correu.</a>
  <li><a href="server-request">Demanant informes d'error per correu.</a>
#  <li><a href="db/ix/full.html">Full list of outstanding and recent bug
#      reports.</a>
#  <li><a href="db/ix/packages.html">Packages with bug reports.</a>
#  <li><a href="db/ix/maintainers.html">Maintainers of packages with bug
#      reports.</a>
</ul>
