#use wml::debian::template title="Debian BTS &mdash; targeta de refer�ncia pel servidor de correu" NOHEADER=yes NOCOPYRIGHT=true
#use wml::debian::translation-check translation="1.37" maintainer="Jordi Mallach"

<h1>Targeta de refer�ncia pels servidors de correu</h1>

<p>Tota la documentaci� dels servisdors de correu est� disponible en
la WWW, en els fitxers
<a href="server-request">bug-log-mailserver.txt</a> i
<a href="server-control">bug-maint-mailcontrol.txt</a> o enviant la
paraula <code>help</code> a cada servidor de correu.</p>

<h2>Sinopsi dels comandaments disponibles en <code>request@bugs.debian.org</code></h2>

<ul>
<li><code>send</code> <var>n�mero-d'error</var></li>
<li><code>send-detail</code> <var>n�mero-d'error</var></li>
<li><code>index</code> [<code>full</code>]</li>
<li><code>index-summary by-package</code></li>
<li><code>index-summary by-number</code></li>
<li><code>index-maint</code></li>
<li><code>index maint</code> <var>mantenidor</var></li>
<li><code>index-packages</code></li>
<li><code>index packages</code> <var>paquet</var></li>
<li><code>send-unmatched</code> [<code>this</code>|<code>0</code>]</li>
<li><code>send-unmatched</code> <code>last</code>|<code>-1</code></li>
<li><code>send-unmatched</code> <code>old</code>|<code>-2</code></li>
<li><code>getinfo</code> <var>nom-fitxer</var> <small>(ftp.debian.org/debian/doc/*)</small></li>
<li><code>help</code></li>
<li><code>refcard</code></li>
<li><code>quit</code>|<code>stop</code>|<code>thank</code>...|<code>--</code>...</li>
<li><code>#</code>... <em>(comment)</em></li>
<li><code>debug</code> <var>nivell</var></li>
</ul>

<h2>Sinopsi dels comandaments extra disponibles en <code>control@bugs.debian.org</code></h2>

<ul>
<li><code>reassign</code> <var>n�mero-error</var> <var>paquet</var>
 [ <var>versi�</var> ]</li>
<li><code>severity</code> <var>n�mero-error</var> <var>gravetat</var></li>
<li><code>reopen</code> <var>n�mero-error</var>
 [ <var>adre�a-originador</var> | <code>=</code> | <code>!</code> ]</li>
<li><code>found</code> <var>n�mero-error</var> [ <var>versi�</var> ]</li>
<li><code>notfound</code> <var>n�mero-error</var> <var>versi�</var></li>
<li><code>submitter</code> <var>n�mero-error</var>
 <var>adre�a-originador</var> | <code>!</code></li>
<li><code>forwarded</code> <var>n�mero-error</var> <var>adre�a</var></li>
<li><code>notforwarded</code> <var>n�mero-error</var></li>
<li><code>owner</code> <var>n�mero-error</var>
 <var>adre�a</var> | <code>!</code></li>
<li><code>noowner</code> <var>n�mero-error</var></li>
<li><code>retitle</code> <var>n�mero-error</var> <var>nou-t�tol</var></li>
<li><code>clone</code> <var>n�mero-error</var> <var>ID nova</var> [ <var>noves ID</var> ... ]</li>
<li><code>merge</code> <var>n�mero-error</var> <var>n�mero-error</var> ...</li>
<li><code>unmerge</code> <var>n�mero-error</var></li>
<li><code>forcemerge</code> <var>n�mero-error</var> <var>n�mero-error</var> ...</li>
<li><code>tag</code> <var>n�mero-error</var>
  [ <code>+</code> | <code>-</code> | <code>=</code> ] <var>etiqueta</var> [ <var>etiqueta</var> ... ]</li>
<li><code>block</code> <var>n�mero-error</var> <code>by</code> <var>error</var> ...</li>
<li><code>unblock</code> <var>n�mero-error</var> <code>by</code> <var>error</var> ...</li>
<li><code>close</code> <var>n�mero-error</var> [ <var>versi�-arreglada</var> ]
 <strong>(obsoleta &ndash; haureu d'enviar el motiu per separat al originador,
 vegeu �<a href="Developer#closing">tancant informes d'error</a>�)</strong></li>
</ul>

<p><code>reopen</code> amb <code>=</code> o sense l'adre�a de
l'originador (l'originador �s qui el va enviar originalment);
<code>!</code> us marcar� com la persona que l'heu reobert.</p>

<p>Les <a href="Developer#severities">gravetats</a> s�n <code>critical</code>,
<code>grave</code>, <code>serious</code>, <code>important</code>,
<code>normal</code>, <code>minor</code> i <code>wishlist</code>.</p>

<p>Actualment les <a href="Developer#tags">etiquetes</a> inclouen
<code>patch</code>, <code>wontfix</code>, <code>moreinfo</code>,
<code>unreproducible</code>, <code>help</code>, <code>pending</code>,
<code>fixed</code>, <code>security</code>, <code>upstream</code>,
<code>confirmed</code>, <code>fixed-upstream</code>,
<code>fixed-in-experimental</code>, <code>d-i</code>, <code>ipv6</code>,
<code>lfs</code>, <code>l10n</code>, <code>potato</code>, <code>woody</code>,
<code>sarge</code>, <code>sarge-ignore</code>, <code>etch</code>,
<code>etch-ignore</code>, <code>sid</code> i <code>experimental</code>.</p>

<h2>Sinopsi d'adreces d'enviament i seguiment</h2>

<ul>
  <li><var>nnn</var>[ <code>-submit</code> | ]</li>
  <li><var>nnn</var><code>-maintonly</code></li>
  <li><var>nnn</var><code>-quiet</code></li>
  <li><var>nnn</var><code>-forwarded</code></li>
  <li><var>nnn</var><code>-request</code></li>
  <li><var>nnn</var><code>-submitter</code></li>
  <li><var>nnn</var><code>-done</code></li>
  <li><var>nnn</var><code>-close</code></li>
  <li><var>nnn</var><code>-subscribe</code></li>
</ul>

<hr />

#use "otherpages.inc"

#use "$(ENGLISHDIR)/Bugs/footer.inc"
