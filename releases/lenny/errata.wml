#use wml::debian::template title="Debian GNU/Linux 5.0 -- Errades" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="1.3" maintainer="Guillem Jover"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>

<hrline>


<toc-add-entry name="security">Problemes de seguretat</toc-add-entry>

<p>L'equip de seguretat de Debian s'encarrega d'actualitzar els paquets de
la versi� estable en els quals s'hagin identificat problemes relacionats
amb la seguretat. Si us plau, consulteu les <a href="$(HOME)/security/">
p�gines de seguretat</a> per a informaci� relativa a qualsevol problema
de seguretat identificat a <q>lenny</q>.</p>

<p>Si useu APT, podeu afegir la seg�ents l�nia al vostre fitxer
<tt>/etc/apt/sources.list</tt> per a poder accedir a les �ltimes
actualitzacions de seguretat:</p>

<pre>
  deb http://security.debian.org/ lenny/updates main contrib non-free
</pre>

<p>Despr�s d'aix�, executeu <kbd>apt-get update</kbd> seguit de
<kbd>apt-get upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Revisions</toc-add-entry>

<p>De vegades, en el cas de diversos problemes cr�tics o actualitzacions de
seguretat, la distribuci� ja llan�ada �s actualitzada. Generalment
aix� s'indicar� com noves revisions.</p>

#<ul>
#  <li>La primera revisi�, 4.0r1 es va llan�ar el
#      <a href="$(HOME)/News/2007/20070817">17 d'agost de 2007</a>.</li>
#</ul>

<ifeq <current_release_lenny> 5.0.0 "

<p>Encara no hi ha revisions per a Debian 5.0.</p>" "

<p>Vegeu la <a
href=http://ftp.es.debian.org/debian/dists/lenny/ChangeLog>
ChangeLog</a>
pels detalls del canvis entre 5.0.0 i <current_release_lenny/>.</p>"/>

<p>Les correccions a la distribuci� estable llan�ada, sovint entren
en un per�ode de probes abans d'�sser acceptats a l'arxiu. Tot i
aix�, aquestes correccions estan disponibles al directori
<a href="http://ftp.debian.org/debian/dists/lenny-proposed-updates/">
dists/lenny-proposed-updates</a> de qualsevol r�plica de l'arxiu de
Debian.</p>

<p>Si useu APT per a actualitzar els vostres paquets, podreu
instal�lar les actualitzacions proposades afegint la seg�ent l�nia
al vostre fitxer <tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# actualitzacions proposades per a les revisions de 5.0
  deb http://ftp.es.debian.org/debian proposed-updates main contrib non-free
</pre>

<p>Despr�s d'aix�, executeu <kbd>apt-get update</kbd> seguit de
<kbd>apt-get upgrade</kbd>.</p>


<toc-add-entry name="installer">Sistema d'instal�laci�</toc-add-entry>

<p>
Per a m�s informaci� relacionada amb errades i actualitzacions per al sistema
d'instal�laci�, vegeu la p�gina d'<a href="debian-installer/">informaci�
d'instal�laci�</a>.
</p>
