#use wml::debian::template title="Arxius de distribucions"
#use wml::debian::toc
#use wml::debian::translation-check translation="1.16" maintainer="Guillem Jover"

<toc-display />

<toc-add-entry name="old-archive">debian-archive</toc-add-entry>

<p>Si necessiteu accedir a alguna de les distribucions antigues de Debian,
podeu trobar-les a l'<a href="http://archive.debian.org/debian/">Arxiu
de Debian</a>, <tt>http://archive.debian.org/debian/</tt>.</p>

<p>Els llan�aments estan emmagatzemats pel seu nom en clau sota el
directori dists/.</p>
<ul>
  <li><a href="../releases/sarge/">sarge</a> �s Debian 3.1</li>
  <li><a href="../releases/woody/">woody</a> �s Debian 3.0</li>
  <li><a href="../releases/potato/">potato</a> �s Debian 2.2</li>
  <li><a href="../releases/slink/">slink</a> �s Debian 2.1</li>
  <li><a href="../releases/hamm/">hamm</a> �s Debian 2.0</li>
  <li>bo   �s Debian 1.3</li>
  <li>rex  �s Debian 1.2</li>
  <li>buzz �s Debian 1.1</li>
</UL>

<p>Al cap del temps caducarem els paquets binaris per als llan�aments antics.
Actualment tenim binaris disponibles per a
<i>sarge</i>, <i>woody</i>, <i>potato</i>, <i>slink</i>, <i>hamm</i>
i <i>bo</i>, i nom�s codi font per als altres llan�aments.</p>

<p>Si esteu emprant l'APT, les entrades rellevants per a sources.list s�n com:</p>
<pre>
  deb http://archive.debian.org/debian/ hamm contrib main non-free
</pre>
<p>o</p>
<pre>
  deb http://archive.debian.org/debian/ bo bo-unstable contrib main non-free
</pre>

<p>A continuaci� la llista de r�pliques que inclouen l'arxiu:</p>

#include "$(ENGLISHDIR)/distrib/archive.mirrors"
<archivemirrors>

<toc-add-entry name="non-us-archive">Arxiu debian-non-US</toc-add-entry>

<p>En el passat, hi havia programari empaquetat per Debian que no es podia
distribuir a US (i altres pa�sos) degut a restriccions d'exportaci� de xifrat
o patents de programari. Debian mantenia un arxiu especial anomenat
<q>non-US</q></p>.

<p>Aquests paquets es van incorporar en l'arxiu �main� a Debian 3.1 i l'arxiu
debian-non-US s'ha aturat; ara est� realment <em>arxivat</em>, incorporat
als arxius de archive.debian.org.</p>

<p>Encara s�n disponibles a la maquina archive.debian.org.
Els m�todes d'acc�s disponibles s�n:</p>
<blockquote><p>
<a href="ftp://archive.debian.org/debian-non-US/">ftp://archive.debian.org/debian-non-US/</a><br>
<a href="http://archive.debian.org/debian-non-US/">http://archive.debian.org/debian-non-US/</a><br>
rsync://archive.debian.org/debian-non-US/  (limited)
</p></blockquote>

<p>Per usar aquests paquets amb APT, les entrades rellevants per a
sources.list s�n com:</p>

<pre>
  deb http://archive.debian.org/debian-non-US/ woody/non-US main contrib non-free
  deb-src http://archive.debian.org/debian-non-US/ woody/non-US main contrib non-free
</pre>

<p>Les r�pliques contenien non-US en el directori <tt>debian-non-US</tt>.
La seg�ent, �s una llista de r�pliques que inclouen l'arxiu non-US:</p>

#include "$(ENGLISHDIR)/distrib/archive-nonus.mirrors"
<archivenonusmirrors>

