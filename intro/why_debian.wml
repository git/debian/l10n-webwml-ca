#use wml::debian::template title="Motius per a escollir Debian"
#use wml::debian::translation-check translation="1.57" maintainer="Jordi Mallach"
#include "$(ENGLISHDIR)/releases/info"

<p>Gr�cies per tindre en consideraci� el fet d'usar Debian GNU/Linux per
a fer funcionar la vostra m�quina.  Si encara no n'esteu conven�uts de
per qu� escollir Debian, considereu el seg�ent:
</p>

<dl>
<dt><strong>Est� mantingut pels seus usuaris.</strong></dt>

    <dd>Si res necessita ser solventat o millorat, simplement ho fem.</dd>

<dt><strong>Un suport incomparable</strong></dt>

    <dd>El correu enviat a les <a href="$(HOME)/MailingLists/">llistes de correu</a>
    sovint aconsegueix una resposta en 15 minuts (o menys), de franc i per la
    gent que la desenvolupa. Compareu aix� amb el t�pic nombre de tel�fon de
    suport: hores esperant, gastant diners, sols per a aconseguir a alg� que
    no coneix el sistema el suficient per a atendre la vostra pregunta.</dd>

<dt><strong>No estarieu sols en la vostra elecci�</strong></dt>

    <dd>Un ample n�mero de organitzacions i individus utilitzen Debian. Veieu
    la nostra p�gina <a href="$(HOME)/users/">Qu� usa Debian?"</a>, una
    descripci� d'alguns llocs de perfil alt que usen Debian i han enviat una
    curta descripci� de com usen Debian i per qu�.</dd>

<dt><strong>El millor sistema d'empaquetament del m�n.</strong></dt>

    <dd>Cansat dels vells fitxers de programari de tres versions anteriors al
    altual rondant pel vostre sistema? O d'instal�lar una pe�a
    de programari sols per a trobar-vos amb que aquest causa problemes en
    el sistema per conflictes de programari? Dpkg, �s el s�lid sistema
    d'empaquetament de Debian, s'encarregar� d'aquestes q�estions per
    a vosaltres.</dd>

<dt><strong>Facilitat d'instal�laci�</strong></dt>

    <dd>Si heu sentit que GNU/Linux �s dif�cil d'instal�lar, llavors es que no
    heu provat Debian �ltimament. Estem constantment millorant el proc�s
    d'instal�laci�. Podeu instal�lar directament des de CD, DOS, disquets
    o fins i tot des de la mateixa xarxa.</dd>

<dt><strong>Una incre�ble quantitat del programari</strong></dt>

    <dd>Debian us proveeix de m�s de <a href="$(HOME)/distrib/packages">\
    <packages_in_stable> peces diferents de programari</a>. Cada bit de les
    quals �s <a href="free">lliure</a>.
    Si teniu programari propietari que esteu executant sota GNU/Linux, encara
    el podreu usar; de fet, en Debian tamb� podeu tindre un instal�lador que
    autom�ticament us ho instal�li i configure tot.</dd>

<dt><strong>Paquets ben integrats</strong></dt>

    <dd>Debian sobresurt de totes les altres distribucions en com estan
    integrats els seus paquets. Donat que tot el programari es empaquetat
    segons un grup coherent, no tant sols els podeu trobar tots en un mateix
    lloc, tamb� podeu estar-ne ben segurs de que hem eliminat totes les
    q�estions referents a complexes depend�ncies. Creiem que el format deb
    te alguns avantatges sobre el format rpm, la integraci� entre els
    paquets �s el que fa a Debian m�s robusta.
    </dd>

<dt><strong>El codi font</strong></dt>

    <dd>Si sou un desenvolupador de programari, apreciareu el fet de tindre
    disponibles un centenar d'utilitats i d'idiomes de desenvolupament, a m�s
    de milions de l�nies de codi del sistema base. Tot el programari de la
    distribuci� principal segueix els criteris dels
    <a href="$(HOME)/social_contract#guidelines">Principis del programari
    lliure de Debian (DFSG)</a>. Aix� significa que podeu usar aquest codi per
    a estudiar-lo o incorporar-lo a nous projectes de programari lliure. Tamb�
    hi han en abund�ncia utilitats i codi adequat per a ser usat en projectes
    propietaris.</dd>

<dt><strong>Facilitat d'actualitzaci�</strong></dt>

    <dd>Degut al nostre sistema d'empaquetament, actualitzar a una nova versi�
    de Debian �s un moment. Senzillament executeu <tt>apt-get update ; apt-get
    dist-upgrade</tt> (o <tt>aptitude update; aptitude dist-upgrade</tt> en
    les versions m�s noves de Debian) i podreu actualitzar des d'un CD en
    q�esti� de minuts, tot i que tamb� podeu apuntar a apt cap a una de les
    300 <a href="$(HOME)/mirror/list">r�pliques</a> de Debian i actualitzar-vos
    des de la xarxa.</dd>

<dt><strong>El sistema de seguiment d'errors</strong></dt>

    <dd>El <a href="http://bugs.debian.org/">sistema de seguiment d'errors</a>
    de Debian es troba a disposici� del p�blic. No intentem amagar-nos de la
    realitat de que el programari no sempre funciona com el usuari desitja.
    Encoratgem als usuaris per a que reportin informes d'error i aquests seran
    informats quan aquest error es solucioni. Aquest sistema permet a Debian
    respondre als problemes amb rapidesa i honestitat.</dd>

</dl>

<p>Si encara no sou usuari de GNU/Linux, tamb� podeu disfrutar dels seg�ents
avantatges:
</p>

<dl>
<dt><strong>Estabilitat</strong></dt>

    <dd>Hi han molts casos de m�quines que funcionen durant m�s d'un any
    ininterrompudament sense necessitat de reiniciar.  Aix� com tamb�
    n'hi han que sols s�n reiniciats degut a una falla del subministrament
    el�ctric o una actualitzaci� del maquinari. Compareu aix� amb d'altres
    sistemes que �s colapsen m�ltiples vegades al dia.</dd>

<dt><strong>R�pid i lleuger de mem�ria</strong></dt>

    <dd>D'altres sistemes operatius poden ser r�pids en una o dues �rees,
    per� al estar basat en GNU/Linux, Debian �s lleuger i humil. El
    programari de Windows s'executa des de GNU/Linux usant un emulador, el
    qual de vegades �s <strong>m�s r�pid</strong> que el mateix programa
    executat en el seu entorn original.</dd>

<dt><strong>Els controladors per a molt del maquinari �s escrit pels usuaris
    de GNU/Linux, no pel fabricant.</strong></dt>

    <dd>Tot i que aix� pot suposar endarreriments abans de que aquest nou
    maquinari sigui suportat i d'altra que no tingui suport, tamb� habilita
    el suport del maquinari molt despr�s de que el fabricant n'hagi aturat
    la producci� o que aquest s'hagi retirat del negoci. La experi�ncia
    ens ha demostrat que els controladors de Codi Obert normalment solen
    ser millors que els propietaris.</dd>

<dt><strong>Un bon sistema de seguretat</strong></dt>

    <dd>Debian i la comunitat de software lliure s�n molt reactius a
    l'hora d'asegurar-se que les solucions als problemes de seguretat
    siguin incloses en la distribuci� r�pidament. Normalment els paquets
    corregits s�n inclosos en q�esti� de pocs dies. La disponibilitat del
    codi font permet que la seguretat en Debian sigui evaluada obertament
    prevenint-nos aix� de que s'implementin models pobres de seguretat.
    Tant mateix, la major part de projectes de software lliure tenen
    sistemes de revisi� entre iguals, que prevenen en primer lloc
    l'introducci� de problemes potencials de seguretat.</dd>

<dt><strong>Programari de seguretat</strong></dt>

    <dd>Molts desconeixen que qualsevol cosa que s'envi� a la xarxa pot ser
    llegida per qualsevol m�quina entre la vostra i la receptora. Debian
    disposa de paquets del fam�s programari GPG (i PGP) amb el qual el correu
    pot ser enviat privadament a d'altres usuaris. A m�s de ssh, que ens
    permet crear connexions segures cap a d'altres m�quines que tamb� tinguin
    el ssh instal�lat.</dd>

</dl>

<p>Per descomptat, Debian no �s perfecte. Hi ha tres �rees que s�n causes
comuns de queixes:
</p>

<dl>

<dt><em>�L'abs�ncia del programari comercial m�s popular�.</em></dt>

    <dd><p>�s ben cert que part del programari m�s conegut, no est� disponible
    per a GNU/Linux.  Encara que, hi han programes que suplanten
    a molts d'aquests, dissenyats per a mimetitzar moltes de les millors
    caracter�stiques del programari propietari, amb el valor afegit de que s�n
    <a href="free">programari lliure</a>.</p>

    <p>L'abs�ncia de paquets d'ofim�tica no tindria que ser un problema ja que
    Debian inclou tres paquets d'ofim�tica composats completament per
    <a href="free">programari lliure</a>,
    <a href="http://www.openoffice.org/">OpenOffice</a>,
    <a href="http://www.koffice.org/">KOffice</a> i
    <a href="http://www.gnome.org/gnome-office/">GNOME Office</a>.
    </p>

    <p>Tamb� hi ha varios paquets d'ofim�tica propietaris disponibles:
    <a href="http://www.vistasource.com/page.php?id=7">Applixware
    (Anyware)</a>, <a href="http://www.sun.com/staroffice/">StarOffice</a>,
    <a href="http://www.hancom.com/">Hancom Office</a>,
    <a href="http://xibios.free.fr/">Axene</a> i d'altres.
    </p>

    <p>Pels qui estan interessats en bases de dades, Debian es distribueix
    amb els programes de bases de dades m�s populars:
    <a href="http://www.mysql.com/">mySQL</a> i
    <a href="http://www.postgresql.org/">PostgreSQL</a>.
    <a href="http://www.sapdb.org/">SAP DB</a>,
    <a href="http://otn.oracle.com/tech/linux/content.html">Oracle</a>,
    <a href="http://www-3.ibm.com/software/data/informix/">Informix</a>,
    <a href="http://www.ibm.com/software/data/db2/linux/">IBM DB2</a>,
    i algunes m�s que estan disponibles per a GNU/Linux.
    </p>

# This figures need to be updated:
    <p>Altre programari propietari est� apareixent en major quantitat, a mesura
    que les companyies descobreixen el poder de GNU/Linux i el seu mercat
    quasi verge amb una base d'usuaris en constant creiximent (com GNU/Linux
    �s de distribuci� lliure, els n�meros de vendes no es poden utilitzar per
    a fer estimacions. Les millors estimacions s�n que GNU/Linux t� el 5% del
    mercat, l'equivalent a 15 milions d'usuaris a principis de 2001).
    </p>
</dd>

<dt><em>�GNU/Linux �s dif�cil de configurar�.</em></dt>

    <dd>Noteu que es parla de configurar, no instal�lar, donat que algunes
    persones troben que la instal�laci� inicial de Debian �s m�s senzilla
    que la del Windows.  Una part del maquinari (les impressores per exemple)
    podrien ser m�s f�cils de configurar.  Tamb�, algun programari pot tindre
    algun script que us guiar� a trav�s de la configuraci� (al menys per a
    les configuracions m�s comuns).  Aquesta �s una �rea en la que s'est�
    treballant.
</dd>

<dt><em>�No est� suportat tot el maquinari�.</em></dt>

    <dd>En particular, el maquinari realment nou, realment antic o rar.
    Tamb� el maquinari que dep�n d'un complex programari en el
    �controlador� que els fabricants sols subministren per a plataformes
    Windows (el programari de m�dems o alguns dispositius wifi per a port�tils
    per exemple). Encara que, en molts casos, hi ha disponible maquinari
    equivalent que funciona amb GNU/Linux. Algun maquinari no est� suportat
    degut a que el fabricant va decidir no deixar les especificacions del
    maquinari disponibles. Aquesta tamb� �s una �rea en la que s'est�
    treballant.</dd>

</dl>

<p>Si tot el que s'us ha exposat fins ara no fos suficient per a
convencer-us d'usar Debian, tingueu en consideraci� el seg�ent:
el baix cost (tan petit com el cost d'una connexi� de xarxa), f�cil
instal�laci� i multitasca que us permetr� duplicar la vostra
productivitat. Com us pod�eu permetre no escollir-la?
</p>

# <p>Enlla�os relacionats:
# http://alexsh.hectic.net/debian.html
