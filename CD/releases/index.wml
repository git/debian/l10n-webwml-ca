#use wml::debian::cdimage title="Informaci� sobre imatges de CD oficials de Debian GNU/Linux" BARETITLE=true
#use wml::debian::translation-check translation="1.31" maintainer="Jordi Mallach"

<p>Aquesta p�gina cont� informaci� hist�rica d'�ltima hora en relaci� a les imatges
oficials de Debian en CD.</p>

<em>Est� enfocada en problemes espec�fics a imatges
en CD </em> - llegiu <a href="$(HOME)/releases/">informaci� general sobre el
llan�ament</a> per a problemes que no siguin limitats a aquelles persones que
instal�lin des de CD-ROM.</p>

<p>Informaci� sobre problemes d'instal�laci� de versions posteriors a
les llistades abaix est� disponible a la p�gina d'<q>informaci� de la
instal�laci�</q> per a cada llan�ament. Informaci� per a l'actual
versi� estable es pot trobar
<a href="$(HOME)/releases/stable/debian-installer/">aqu�</a>.</p>

<p>
<strong>La seg�ent llista ja no s'actualitza.</strong>
</p>

<hrline />


<dl>
# ------------------------------------------------------------
  <release-notes title="Official Debian 4.0 rev2" version="4.0 rev2">
    <p>Cap problema conegut.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 4.0 rev1" version="4.0 rev1">
    <p>Cap problema conegut.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 4.0 rev0" version="4.0 rev0">

    <p>Si instal�leu des de CD-ROM o DVD i trieu tamb� fer servir una
    r�plica durant la instal�laci�, hi ha la possibilitat que despr�s de la
    instal�laci� hi hagi l�nies en el fitxer <tt>/etc/apt/sources.list</tt>
    referint-se a <q>sarge</q> en comptes d'<q>etch</q>.<br />
    Aix� nom�s pot succeir si la r�plica que heu seleccionat no est�
    al dia i encara t� sarge com a versi� estable. Es recomana als usuaris
    que instal�leu etch des de CD/DVD poc despr�s del llan�ament que
    comproveu el fitxer <tt>sources.list</tt> despr�s de la instal�laci�
    i reemplaceu <q>sarge</q> per <q>etch</q> si �s necessari.</p>

  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 3.1 rev6a" version="3.1 rev6a">
    <p>Cap problema conegut.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 3.1 rev6" version="3.1 rev6">
    <p>S'ha descobert un error a les imatges d'instal�laci� de CD/DVD 3.1r6:
    les instal�lacions fallarien immediatament degut a la manca d'un enlla�
    simb�lic a <q>oldstable</q>. No obstant, les imatges de CD/DVD
    d'actualitzaci� funcionen correctament.</p>
    <p>Aquest problema es va resoldre en les imatges 3.1r6a.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 3.1 rev5" version="3.1 rev5">

    <p>Durant la instal�laci� a les arquitectures i386, hppa, ia64 i
    s390, l'instal�lador podria seleccionar un nucli incorrecte per al
    vostre sistema.<br />
    Podeu solucionar temporalment aquest problema arrencant l'instal�lador
    amb el par�metre <tt>debconf/priority=medium</tt>. Aix� far� que es mostri
    una llista completa del nuclis disponibles, de la que podeu seleccionar
    manualment el sabor apropiat per al vostre sistema.</p>

    <p>Perdoneu les mol�sties que aix� pugui ocasionar; esperem solucionar
    aquest problema per a r6.

  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 3.1 rev4" version="3.1 rev4">
    <p>Cap problema conegut.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 3.1 rev3" version="3.1 rev3">

    <p>El CD#1 cont� fitxers addicionals que s'haurien d'haver esborrat,
    per� �s van descuidar al llan�ar 3.1r3. Aix� causa dos problemes:</p>

    <ul>
      <li>El nucli instal�lat usant aquestes imatge, en alguns casos,
      no ser� l'�ltima versi� al CD, ser� l'anterior. Actualitzeu el
      nucli espec�ficament despr�s de la instal�laci� i no suposar� cap
      problema.</li>

      <li>Ja que els fitxers addicionals al CD#1 ocupen espai extra,
      algunes de les tasques normals d'instal�laci� ja no hi cabran.
      Si voleu instal�lar totes les tasques nom�s des de CD, tamb�
      necessitareu fer servir el CD#2.</li>
    </ul>

    <p>Perdoneu les mol�sties que aix� pugui ocasionar; esperem solucionar
    aquest problema per a r4. Les imatges de DVD i les instal�lacions
    per xarxa no estan afectades.</p>

  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 3.1 rev2" version="3.1 rev2">
    <p>Cap problema conegut.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 3.1 rev1" version="3.1 rev1">
    <p>Cap problema conegut.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 3.1 rev0a" version="3.1 rev0a">
    <p>El README del CD especifica que �s una versi� beta no oficial.
    El README �s incorrecte, <em>�s</em> el llan�ament oficial dels CD.
    Perdoneu la confusi�.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 3.1 rev0" version="3.1 rev0">
    <p>Instal�lacions resultants d'aquestes imatges
    <a href="http://lists.debian.org/debian-devel-announce/2005/06/msg00003.html">
    instal�laran una l�nia incorrecta per a les actualitzacions de seguretat
    a /etc/apt/sources.list</a>. La reconstrucci� completa est� en progr�s.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 3.0 rev6" version="3.0 rev6">
    <p>Cap problema conegut.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 3.0 rev5" version="3.0 rev5">
    <p>Cap problema conegut.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 3.0 rev4" version="3.0 rev4">
    <p>Cap problema conegut.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 3.0 rev3" version="3.0 rev3">
    <p>Cap problema conegut.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 3.0 rev2" version="3.0 rev2">
    <p>Cap problema conegut.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 3.0 rev1" version="3.0 rev1">
    <p>Cap problema conegut.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 3.0 rev0" version="3.0 rev0">
    <p>El README del CD indica que �s una versi� beta no oficial. El
    README est� equivocat, <em>�s</em> el CD oficial. Perdoneu per la
    confusi�.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 2.2 rev7" version="2.2 rev7">
    <p>Cap problema conegut.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 2.2 rev6" version="2.2 rev6">
    <p>Cap problema conegut.</p>
  </release-notes>

# ------------------------------------------------------------
  <release-notes title="Official Debian 2.2 rev5" version="2.2 rev5">
    <p>Cap problema conegut.</p>
  </release-notes>

# ------------------------------------------------------------
  <dt><strong>Official Debian 2.2 rev4 i rev4.1</strong></dt>
    <dd>\
    <p>Els CD originals 2.2rev4 per a l'arquitectura powerpc 
    no eren arrencables. S'han regenerat - el llan�ament 2.2rev4.1 
    resultant no difereix del 2.2rev4 excepte en powerpc.</p>\
    </dd>

# ------------------------------------------------------------
  <dt><strong>Official Debian 2.2 rev3</strong></dt>
    <dd>
    <p>Sembla que els port�tils Toshiba tenen problemes per a arrencar els CD
    perqu� la seva BIOS
    <a href="http://lists.debian.org/debian-devel-0104/msg01326.html">no
    suporta</a> imatges d'arrancada de 2.88 MiB. La manera m�s f�cil per a
    comen�ar a instal�lar Debian en aquestes maquines es executar
    <tt>boot.bat</tt> des del directori <tt>install/</tt> del CD Binary-1.</p>

    <p>No tots els paquets de la secci� <q>contrib</q> s�n inclosos als CD,
    perqu� depenen de paquets non-free que tampoc s�n als CD.</p>

    <p>Els problemes amb el controlador de ratol� en mode text <tt>gpm</tt>
    encara no han estat resolts. Mireu a la secci� <q>2.2 rev0</q> m�s avall
    per a m�s informaci�.</p>

  </dd>

# ------------------------------------------------------------
  <dt><strong>Official Debian 2.2 rev2</strong></dt>
  <dd>
    <p>No tots els paquets de la secci� <q>contrib</q> s�n inclosos als CD,
    perqu� depenen de paquets non-free que tampoc s�n als CD.</p>

    <p>Els problemes amb el controlador de ratol� en mode text <tt>gpm</tt>
    encara no han estat resolts. Mireu a la secci� <q>2.2 rev0</q> m�s avall
    per a m�s informaci�.</p>
  </dd>

# ------------------------------------------------------------
  <dt><strong>Official Debian 2.2 rev1</strong></dt>
  <dd><p>No s'han creat imatges en CD pel llan�ament 2.2 rev1.</p></dd>

# ------------------------------------------------------------
  <dt><strong>Official Debian 2.2 rev0</strong></dt>
    <dd>
    <p>No tots els paquets de la secci� <q>contrib</q> s�n inclosos als CD,
    perqu� depenen de paquets non-free que tampoc s�n als CD.</p>

    <p><strong>i386</strong>: Hi ha alguns problemes amb el controlador de
    ratol� <tt>gpm</tt> en mode-text mentre corre el sistema X Window. La
    soluci� m�s f�cil es treure la l�nia
    <tt>repeat_type=<em>&lt;quelcom&gt;</em></tt> de <tt>/etc/gpm.conf</tt>,
    executar <q><tt>/etc/init.d/gpm&nbsp;restart</tt></q> i llavors tornar
    a iniciar X. Altres solucions s�n possibles, pregunteu al
    <a href="mailto:gpm@packages.debian.org">mantenidor de gpm</a> per a m�s
    documentaci�.</p>

    <p><strong>i386</strong>: La imatge del CD Binary-2 disponible
    anteriorment tenia un problema d'un bit que impedia instal�lar el paquet
    <q><tt>pdksh</tt></q>. Reparar la vostra pr�pia imatge es molt f�cil amb
    el programa
    <a href="http://cdimage.debian.org/~costar/correct_cds/correct-i386-2.c">\
    correct-i386-2.c</a>.</p>

    <p>Gr�cies a <a href="mailto:kteague@sprocket.dhis.net">Ken Teague</a>
    tamb� tenim una
    <a href="http://cdimage.debian.org/~costar/correct_cds/correct-i386-2.zip">versi� per a Windows</a> precompilada; forma d'�s: extreure el <tt>.zip</tt> al
    directori on la sigui la imatge <tt>binary-i386-2.iso</tt>, llavors, en una
    finestra DOS canviar a aquest directori i introduir l'ordre
    <q><tt>correct-i386-2 binary-i386-2.iso</tt></q>.</p>

    <p>Si ja teniu un CD-ROM, el qual �bviament no podeu reparar, la opci� m�s
    f�cil per aconseguir <tt>pdksh</tt> �s
    <a href="http://ftp.debian.org/debian/dists/potato/main/binary-i386/shells/pdksh_5.2.14-1.deb">descarregar-lo</a>
    (212 KiB) i instal�lar-lo amb <q><tt>dpkg -i pdksh_5.2.14-1.deb</tt></q>.
    Per� tamb� podeu copiar el fitxer des del CD a un directori temporal i
    usar el mateix programa
    <a href="http://cdimage.debian.org/~costar/correct_cds/correct-i386-2.c">correct-i386-2.c</a>,
    per� llavors heu de modificar-lo per a que<tt>POS</tt> siga
    <tt>0x64de</tt>.</p>

    <p><strong>PowerPC</strong>: Els CD Binary-1_NONUS i Binary-3 de powerpc
    tamb� estan afectats per un problema d'un bit, impedint que
    <q><tt>smbfs</tt></q> i <q><tt>gimp-manual</tt></q> s'instal�lin.
    Les versions reparades s'estan propagant (lentament) cap a les r�pliques
    per� podeu reparar les vostres imatges molt f�cilment amb els programes
    <a href="http://cdimage.debian.org/~costar/correct_cds/correct-powerpc-1_NONUS.c">correct-powerpc-1_NONUS.c</a>
    i
    <a href="http://cdimage.debian.org/~costar/correct_cds/correct-powerpc-3.c">correct-powerpc-3.c</a>.
    Aquests tamb� contenen informaci� per a reparar els paquets afectats
    individualment quan s�n copiats del CD (cosa bastant �til per al
    <q><tt>gimp-manual</tt></q> que pesa 15 MiB).</p>

    <p><strong>Sparc</strong>: Els CD de 2.2 rev0 tenen un problema quan
    s'arrenquen des del CD Binary-1. Aix� ha estat solucionat a la versi� 2.2
    rev0a (o 2.2_rev0_CDa) dels CD per a sparc.</p>

    <p>Sparc: Si les X no arrenquen correctament, i el missatge d'error
    menciona el ratol�, i <em>no</em> esteu executant el controlador en mode
    text <tt>gpm</tt>, <q><tt>rm -f /dev/gpmdata</tt></q> us pot ajudar.</p>

    <p><strong>Alpha</strong>: Els CD 2.2 rev0 poden mostrar-se problem�tics
    si s'arranquen des del CD Binary-1. Per a solucionar aix�, arrenqueu amb
    <q><tt>-flags i</tt></q>, llavors entreu a l'indicador d'aboot:
    <br />
      <tt>&nbsp;&nbsp;&nbsp;&nbsp;aboot&gt; b /linux
      initrd=/boot/root.bin root=/dev/ram</tt>
    <br />
    Aquest problema s'ha corregit a la versi� 2.2 rev0a (o 2.2_rev0_CDa)
    dels CD per a alpha.</p>
  </dd>

# ------------------------------------------------------------
  <dt><strong>Potato test-cycle-3</strong></dt>
    <dd><p>El controlador de ratol� en mode text <tt>gpm</tt> t� alguns
    problemes. <strong>No</strong> haur�eu d'executar el programa
    <tt>mouse-test</tt>, i tindreu que fer <q><tt>/etc/init.d/gpm&nbsp;stop</tt></q>
    abans d'utilitzar el ratol� a X.</p></dd>

# ------------------------------------------------------------
  <dt><strong>Potato test-cycle-2</strong></dt>
    <dd><p>El controlador de ratol� en mode text <tt>gpm</tt> t� alguns
    problemes. <strong>No</strong> haur�eu d'executar el programa
    <tt>mouse-test</tt>, i tindreu que fer <q><tt>/etc/init.d/gpm&nbsp;stop</tt></q>
    abans d'utilitzar el ratol� a X.</p></dd>

# ------------------------------------------------------------
  <dt><strong>Potato test-cycle-1</strong></dt>
    <dd><p>No hi ha cap informaci� rellevant</p></dd>

</dl>
