#use wml::debian::template title="Centre de traduccions de Debian"
#use wml::debian::translation-check translation="1.29" maintainer="Jordi Mallach"

<p>Aquestes p�gines mostren el nombre de paquets en Debian que estan
preparats per a ser tradu�ts i quants ja ho estan.</p>

<p>Sigueu conscient de que aquest proc�s sols �s una part de la
internacionalitzaci� (que s'abrevia com <i>i18n</i> perqu� hi ha 18 lletres
entre la �i� i la �n�) i la localitzaci� (o <i>l10n</i>).  i18n configura els
mecanismes de globalitzaci� i l10n afegeix les dades per a cada llengua i
pa�s en aquests mecanismes. Per a��, les tasques per fer de l10n depenen
del suport d'i18n. Per exemple, si un suport d'i18n nom�s permet canviar el
text d'un missatge, l10n nom�s voldr� dir traduir aquest text. Si la i18n
permet canviar la manera de mostrar la data, podeu expressar-la de la manera
que es fa en la vostra part del mon. Si la i18n vos permet canviar la
codificaci� de car�cters, l10n �s l'acte de fixar la codificaci� de car�cters
que es requereix per a una llengua determinada. Teniu en compte que el suport
per a codificaci� de car�cters, incloent m�ltiples bytes, ampl�ria doble,
combinacions, bi-direcci�, i etc�tera �s un prerequisit per a totes les altres
parts d'i18n i l10n, incloent traduccions, per a algunes lleng�es (en la major
part no-europees).</p>

<p>l10n i i18n estan lligades, per� les dificultats relacionades amb
cadascuna d'elles s�n molt diferents. No �s massa dif�cil permetre a un
programa canviar el text a mostrar depenent de la configuraci� de l'usuari,
per� fer les traduccions dels missatges porta molt de temps. Per altra part,
fixar la codificaci� dels car�cters �s trivial, per� adaptar el codi per a fer
servir v�ries codificacions de car�cters �s un problema
<a href="$(HOME)/doc/devel-manuals#i18n"><em>molt gran</em></a>.</p>

<p>Ac� podeu fullejar algunes estad�stiques sobre la l10n de Debian:</p>

<ul>
 <li>Estat de �l10n� en els fitxers PO, p.e. com estan de tradu�ts els paquets:
 <ul>
  <li><a href="po/">llistat segons la llengua</a></li>
  <li><a href="po/rank">classificaci� entre lleng�es</a></li>
 </ul></li>
 <li>Estat de �l10n� en les plantilles de Debconf gestionades amb gettext:
 <ul>
  <li><a href="po-debconf/">llistat segons la llengua</a></li>
  <li><a href="po-debconf/rank">classificaci� entre lleng�es</a></li>
  <li><a href="po-debconf/pot">fitxers originals</a></li>
 </ul></li>
 <li><a href="http://ddtp.debian.net/">Estat de �l10n� de les descripcions
         dels paquets de Debian</a></li>
 <li><a href="$(HOME)/devel/website/stats/">Estad�stiques de traducci� del
  lloc web de Debian</a></li>
 <li><a href="http://d-i.alioth.debian.org/l10n-stats/translation-status.html">
  Estad�stiques de traducci� de l'instal�lador de Debian</a></li>
</ul>
